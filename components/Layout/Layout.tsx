import * as React from 'react'
import Link from 'next/link'
import Head from 'next/head'

type TLayoutProps = {
    title?: string
}

export const Layout: React.FunctionComponent<TLayoutProps> = ({
    children,
    title
}) => (
    <React.Fragment>
        <Head>
            <title>{title}</title>
            <meta charSet="utf-8" />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <header>
            <nav>
                <Link href="/">
                    <a>Home</a>
                </Link>
            </nav>
        </header>
        <section className={'content'}>
            {children}
        </section>
    </React.Fragment>
)
