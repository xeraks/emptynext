import * as React from 'react'
import { Layout } from '../components/Layout/Layout';

type TIndexProps = {};

export default function Index(props: TIndexProps) {
    console.log(props);
    return (
        <Layout title="Home">
            <h1>Hello Next.js 👋</h1>
        </Layout>
    )
}
